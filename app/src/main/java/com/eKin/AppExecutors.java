package com.eKin;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class AppExecutors {
    private static AppExecutors instance;

    public static AppExecutors getInstance() {
        if (instance == null) {
            instance = new AppExecutors();
        }
        return instance;
    }

    private final ScheduledExecutorService alertsIO = Executors.newScheduledThreadPool(1);

    public ScheduledExecutorService alertsIO() {
        return alertsIO;
    }
}
