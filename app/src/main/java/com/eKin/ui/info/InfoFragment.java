package com.eKin.ui.info;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.eKin.R;
import com.eKin.service.InfoService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class InfoFragment extends Fragment implements View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ImageView bloodLogo;
    private ListView listView;
    private TextView textViewResult;

    public static Fragment newInstance(String param1, String param2) {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://ekrewni.pl/ekrewni/")
            //.baseUrl("http://10.0.2.2:8080/ekrewni/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

    InfoService infoService = retrofit.create(InfoService.class);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr, container, false);
        Button bloods = view.findViewById(R.id.getBloodGroups);
        Button centers = view.findViewById(R.id.getListOfCenters);
        Button donorInfo = view.findViewById(R.id.yes_Donor);
        Button noDonor = view.findViewById(R.id.no_Donor);

        bloods.setOnClickListener(this);
        centers.setOnClickListener(this);
        donorInfo.setOnClickListener(this);
        noDonor.setOnClickListener(this);

        return view;
    }

    private void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onClick(View v) {
        Fragment fragment = null;
        switch (v.getId()) {
            case R.id.getBloodGroups:
                fragment = new BloodGroupsFragment();
                replaceFragment(fragment);
                break;
            case R.id.getListOfCenters:
                fragment = new CentersFragment();
                replaceFragment(fragment);
                break;
            case R.id.yes_Donor:
                fragment = new DonorsContraindicationsFragment();
                replaceFragment(fragment);
                break;
            case R.id.no_Donor:
                fragment = new DonorsCircumstancesFragment();
                replaceFragment(fragment);
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }
}
