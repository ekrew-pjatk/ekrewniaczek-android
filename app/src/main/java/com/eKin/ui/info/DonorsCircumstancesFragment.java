package com.eKin.ui.info;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.eKin.R;
import com.eKin.models.DonorsInformation;
import com.eKin.service.InfoService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class DonorsCircumstancesFragment extends Fragment {

    private TextView textViewResult;

    private Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://ekrewni.pl/ekrewni/")
            //.baseUrl("http://10.0.2.2:8080/ekrewni/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

    private InfoService infoService = retrofit.create(InfoService.class);

    public DonorsCircumstancesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.info_no_donor_fragment, container, false);

        ListView listView = view.findViewById(R.id.textListNoDonor);

        TextView circulatorySystem = view.findViewById(R.id.circulatorySystem);
        TextView circulatorySystemTxt = view.findViewById(R.id.circulatorySystemTxt);

        TextView nervousSystem = view.findViewById(R.id.nervousSystem);
        TextView nervousSystemTxt = view.findViewById(R.id.nervousSystemTxt);

        TextView pathologicalBleeding = view.findViewById(R.id.pathologicalBleeding);
        TextView pathologicalBleedingTxt = view.findViewById(R.id.pathologicalBleedingTxt);

        TextView deliquium = view.findViewById(R.id.deliquium);
        TextView deliquiumTxt = view.findViewById(R.id.deliquiumTxt);

        TextView digestiveSystem = view.findViewById(R.id.digestiveSystem);
        TextView digestiveSystemTxt = view.findViewById(R.id.digestiveSystemTxt);

        TextView respiratorySystem = view.findViewById(R.id.respiratorySystem);
        TextView respiratorySystemTxt = view.findViewById(R.id.respiratorySystemTxt);

        TextView genitourinarySystem = view.findViewById(R.id.genitourinarySystem);
        TextView genitourinarySystemTxt = view.findViewById(R.id.genitourinarySystemTxt);

        TextView immuneSystem = view.findViewById(R.id.immuneSystem);
        TextView immuneSystemTxt = view.findViewById(R.id.immuneSystemTxt);

        TextView endocrynologySystem = view.findViewById(R.id.endocrynologySystem);
        TextView endocrynologySystemTxt = view.findViewById(R.id.endocrynologySystemTxt);

        TextView bloodSystem = view.findViewById(R.id.bloodSystem);
        TextView bloodSystemTxt = view.findViewById(R.id.bloodSystemTxt);

        TextView dermatology = view.findViewById(R.id.dermatology);
        TextView dermatologyTxt = view.findViewById(R.id.dermatologyTxt);

        TextView agreements = view.findViewById(R.id.agreements);
        TextView agreementsTxt = view.findViewById(R.id.agreementsTxt);

        TextView diabetes = view.findViewById(R.id.diabetes);
        TextView diabetesTxt = view.findViewById(R.id.diabetesTxt);

        TextView cancers = view.findViewById(R.id.cancers);
        TextView cancersTxt = view.findViewById(R.id.cancersTxt);

        TextView zymosis = view.findViewById(R.id.zymosis);
        TextView zymosisTxt = view.findViewById(R.id.zymosisTxt);

        TextView creutzfeld = view.findViewById(R.id.creutzfeld);
        TextView creutzfeldTxt = view.findViewById(R.id.creutzfeldTxt);

        TextView syphilis = view.findViewById(R.id.syphilis);
        TextView syphilisTxt = view.findViewById(R.id.syphilisTxt);

        TextView injection = view.findViewById(R.id.injection);
        TextView injectionTxt = view.findViewById(R.id.injectionTxt);

        TextView mentalDistress = view.findViewById(R.id.mentalDistress);
        TextView mentalDistressTxt = view.findViewById(R.id.mentalDistressTxt);

        TextView transplant = view.findViewById(R.id.transplant);
        TextView transplantTxt = view.findViewById(R.id.transplantTxt);

        TextView anaphylacticReaction = view.findViewById(R.id.anaphylacticReaction);
        TextView anaphylacticReactionTxt = view.findViewById(R.id.anaphylacticReactionTxt);

        return view;
    }

    private void getCircumstancesToBeDonor() {

        Call<List<DonorsInformation>> call = infoService.getCircumstances();
        call.enqueue(new Callback<List<DonorsInformation>>() {

            @Override
            public void onResponse(Call<List<DonorsInformation>> call, Response<List<DonorsInformation>> response) {

                if (!response.isSuccessful()) {
                    textViewResult.setText("Code" + response.code());
                    return;
                }

                List<DonorsInformation> listOfCircumstances = response.body();
                String header = "Kryteria dyskwalifikacji stosowane wobec kandydatów na dawców krwi:" + "\n\n";
                textViewResult.append(header.toUpperCase());
                for (DonorsInformation donorsInformation : listOfCircumstances) {
                    String content = "";
                    content += "• " + donorsInformation.getInfo() + "\n";
                    content += "-----------------------------------------------------" + "\n";
                    textViewResult.append(content);
                }
            }

            @Override
            public void onFailure(Call<List<DonorsInformation>> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });

    }
}
