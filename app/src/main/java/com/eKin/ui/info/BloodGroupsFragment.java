package com.eKin.ui.info;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.eKin.R;
import com.eKin.models.Blood;
import com.eKin.service.InfoService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class BloodGroupsFragment extends Fragment {

    private TextView textViewResult;
    private TableLayout tableLayout;
    private Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://ekrewni.pl/ekrewni/")
            //.baseUrl("http://10.0.2.2:8080/ekrewni/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

    private InfoService infoService = retrofit.create(InfoService.class);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.info_blood_fragment, container, false);
        tableLayout = view.findViewById(R.id.tableForBlood);

        return view;
    }

    private void getBloodGroups() {

        Call<List<Blood>> call = infoService.getAllBloodGroups();
        call.enqueue(new Callback<List<Blood>>() {

            @Override
            public void onResponse(Call<List<Blood>> call, Response<List<Blood>> response) {

                if (!response.isSuccessful()) {
                    textViewResult.setText("Code" + response.code());
                    return;
                }

                List<Blood> bloodGroups = response.body();

                for (Blood blood : bloodGroups) {
                    String content = "";
                    content += "• " + blood.getBloodType() + " rh " + blood.getRh() + "\n";

                    textViewResult.append(content);
                }

            }

            @Override
            public void onFailure(Call<List<Blood>> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });
    }

}
