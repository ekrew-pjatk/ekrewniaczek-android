package com.eKin.ui.info;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.eKin.R;
import com.eKin.models.Center;
import com.eKin.service.InfoService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class CentersFragment extends Fragment {

    private TextView textViewResult;
    private Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://ekrewni.pl/ekrewni/")
            //.baseUrl("http://10.0.2.2:8080/ekrewni/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

    private InfoService infoService = retrofit.create(InfoService.class);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.info_center_fragment, container, false);

        ListView listView = view.findViewById(R.id.textListCenters);
        textViewResult = view.findViewById(R.id.resultCenters);
        getCenters();

        return view;
    }

    private void getCenters() {

        Call<List<Center>> call = infoService.getAllCenters();

        call.enqueue(new Callback<List<Center>>() {
            @Override
            public void onResponse(Call<List<Center>> call, Response<List<Center>> response) {

                if (!response.isSuccessful()) {
                    textViewResult.setText("Code" + response.code());
                    return;
                }

                List<Center> centers = response.body();
                textViewResult.setText(" ");
                for (Center center : centers) {
                    String content = "";
                    content += "• " + center.getAddress().getCity().toUpperCase() + "\n";
                    content += center.getName() + "\n";
                    content += "ul. " + center.getAddress().getStreet() + "\n";
                    content += center.getAddress().getPostalCode() + " " + center.getAddress().getCity() + "\n";
                    content += "tel. " + center.getPhoneNumber() + "\n";
                    content += center.getWebSite() + "\n";
                    content += "e-mail: " + center.getEmail() + "\n";
                    content += "-----------------------------------------------------" + "\n";
                    textViewResult.append(content);


                }
            }

            @Override
            public void onFailure(Call<List<Center>> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });
    }

}
