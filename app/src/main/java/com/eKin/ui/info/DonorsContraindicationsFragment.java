package com.eKin.ui.info;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.eKin.R;
import com.eKin.models.DonorsInformation;
import com.eKin.service.InfoService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class DonorsContraindicationsFragment extends Fragment {

    private TextView textViewResult;

    private Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://ekrewni.pl/ekrewni/")
            //.baseUrl("http://10.0.2.2:8080/ekrewni/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

    private InfoService infoService = retrofit.create(InfoService.class);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.info_yes_donor_fragment, container, false);

        ListView listView = view.findViewById(R.id.textListYesContraidinations);
        TextView age = view.findViewById(R.id.age);
        TextView ageTxt = view.findViewById(R.id.ageTxt);
        TextView bmi = view.findViewById(R.id.bmi);
        TextView bmiTxt = view.findViewById(R.id.bmiTxt);
        TextView bloodPressure = view.findViewById(R.id.bloodPressure);
        TextView bloodPressureTxt = view.findViewById(R.id.bloodPressureTxt);
        TextView pulse = view.findViewById(R.id.pulse);
        TextView pulseTxt = view.findViewById(R.id.pulseTxt);

        return view;
    }


    private void getDonorsContraindications() {

        Call<List<DonorsInformation>> call = infoService.getAllContraidinations();
        call.enqueue(new Callback<List<DonorsInformation>>() {

            @Override
            public void onResponse(Call<List<DonorsInformation>> call, Response<List<DonorsInformation>> response) {

                if (!response.isSuccessful()) {
                    textViewResult.setText("Code" + response.code());
                    return;
                }

                List<DonorsInformation> listOfContraidinationsForDonors = response.body();
                String header = "Kryteria dopuszczania kandydatów do pobierania krwi na podstawie badania przedmiotowego:" + "\n\n";
                textViewResult.append(header.toUpperCase());
                for (DonorsInformation donorsInformation : listOfContraidinationsForDonors) {
                    String content = "";
                    content += "• " + donorsInformation.getInfo() + "\n";
                    content += "-----------------------------------------------------" + "\n";
                    textViewResult.append(content);
                }
            }

            @Override
            public void onFailure(Call<List<DonorsInformation>> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });

    }

}

