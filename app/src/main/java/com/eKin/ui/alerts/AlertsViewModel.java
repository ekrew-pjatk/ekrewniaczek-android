package com.eKin.ui.alerts;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import com.eKin.models.Alert;
import com.eKin.models.Center;
import com.eKin.repositories.AlertRepository;
import com.eKin.repositories.CenterRepository;
import com.eKin.service.ProximityService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.preference.PreferenceManager;

public class AlertsViewModel extends AndroidViewModel {
    private AlertRepository alertsRepository;
    private CenterRepository centerRepository;
    private Location lastLocation;

    private List<String> preferredGroups;
    private List<String> preferredCenters;
    private List<Center> availableCenters;
    private SharedPreferences preferences;

    private MutableLiveData<Boolean> isPerformingQuery = new MutableLiveData<>();
    private String alertsType;

    public AlertsViewModel(@NonNull Application application) {
        super(application);

        alertsRepository = AlertRepository.getInstance();
        centerRepository = CenterRepository.getInstance();
        isPerformingQuery.setValue(false);
        alertsType = initPreference();

        preferences = PreferenceManager.getDefaultSharedPreferences(getApplication());
    }

    LiveData<List<Alert>> getAlerts() {
        return alertsRepository.getAlerts();
    }
    public LiveData<List<Center>> getCenters() {
        return centerRepository.getCenters();
    }
    LiveData<Boolean> isPerformingQuery() {
        return isPerformingQuery;
    }
    LiveData<Boolean> isQueryExhausted() {
        return alertsRepository.isQueryExhausted();
    }

    LiveData<Boolean> isAlertRequestTimeout(){
        return alertsRepository.isRequestTimedOut();
    }

    private String initPreference() {
        SharedPreferences preferences = getApplication().getSharedPreferences("alert_preference", Context.MODE_PRIVATE);
        return preferences.getString("alert_type", "all");
    }

    void setLastLocation(Location location) {
        lastLocation = location;
    }

    void setAlertsType(String alertsType) {
        this.alertsType = alertsType;

        SharedPreferences preferences = getApplication().getSharedPreferences("alert_preference", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("alert_type", alertsType);
        editor.apply();
    }

    String getAlertsType() {
        return alertsType;
    }

    void setAvailableCenters(List<Center> centers) {
        this.availableCenters = centers;
    }

    private void setAlertsPreferences() {
        if (alertsType.equals("proximal")) {
            preferredCenters = getProxmialCenters();
            preferredGroups = getPreferredGroups();
            normalizeEmpty();
        }

        if(alertsType.equals("preferred")) {
            preferredCenters = getPreferredCenters();
            preferredGroups = getPreferredGroups();
            normalizeEmpty();
        }

        if(alertsType.equals("all")) {
            preferredCenters = null;
            preferredGroups = null;
        }
    }

    void getFirstAlerts() {
        isPerformingQuery.setValue(true);

        fetchCenters();
        setAlertsPreferences();

        alertsRepository.getFirstPage(preferredCenters, preferredGroups);
    }


    void getNextAlerts() {
        if(!isPerformingQuery.getValue() && !isQueryExhausted().getValue()) {
            isPerformingQuery.setValue(true);
            alertsRepository.getNextPage(preferredCenters, preferredGroups);
        }
    }

    private void fetchCenters() {
        centerRepository.fetchCenters();
    }

    private List<String> getProxmialCenters() {
        List<String> centers = new ArrayList<>();

        if (ActivityCompat.checkSelfPermission(getApplication(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
            return centers;
        }

        return calculateProximalCenters(lastLocation);
    }

    boolean isFilteringOn() {
        return preferences.getBoolean("alerts_filtering_on",false);
    }
    boolean isByPreferenceOn() {
        return preferences.getBoolean("alerts_preferred",false);
    }

    boolean isByProximityOn() {
        return preferences.getBoolean("alerts_proximal",false) &&
               preferences.getBoolean("gps",false);
    }

    private List<String> getPreferredCenters() {
        return new ArrayList<>(preferences.getStringSet("preferred_centers", Collections.emptySet()));
    }

    private List<String> getPreferredGroups() {
        return new ArrayList<>(preferences.getStringSet("preferred_groups", Collections.emptySet()));
    }

    private int getPreferredRange() {
        return preferences.getInt("alert_range", 0);
    }


    private List<String> calculateProximalCenters(Location location) {
        List<String> centers = new ArrayList<>();

        if(location == null || availableCenters == null) {
            return centers;
        }

        int alertRange = getPreferredRange();
        double userLatitude = location.getLatitude();
        double userLongitude = location.getLongitude();

        for (Center center: availableCenters) {
            double centerLatitude = Double.parseDouble(center.getAddress().getLatitude());
            double centerLongitude = Double.parseDouble(center.getAddress().getLongitude());

            if(ProximityService.calculateDistance(centerLatitude, centerLongitude, userLatitude, userLongitude) <= alertRange) {
                centers.add(center.getName());
            }
        }

        return centers;
    }


    private void normalizeEmpty() {
        //workaround because retrofit can't send empty array list in query by itself

        if(preferredCenters.size() == 0) {
            preferredCenters.add("");
        }

        if(preferredGroups.size() == 0) {
            preferredGroups.add("");
        }
    }
}