package com.eKin.ui.alerts;

import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import com.eKin.R;
import com.eKin.models.Alert;
import com.eKin.models.Center;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import java.time.format.DateTimeFormatter;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class AlertDetails extends DialogFragment {

    private FusedLocationProviderClient locationClient;

    public AlertDetails() { }

    static AlertDetails newInstance(Alert alert, Center center) {
        AlertDetails frag = new AlertDetails();
        Bundle args = new Bundle();

        args.putParcelable("alert", alert);
        args.putParcelable("center", center);

        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.alert_details, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Alert alert = getArguments().getParcelable("alert");
        Center center = getArguments().getParcelable("center");

        TextView message = view.findViewById(R.id.alert_message);
        TextView centerName = view.findViewById(R.id.alert_center_name);
        TextView bloodGroup = view.findViewById(R.id.alert_details_group);
        TextView date = view.findViewById(R.id.alert_details_date);
        TextView address = view.findViewById(R.id.alert_detials_address);
        TextView phone = view.findViewById(R.id.alert_detials_phone);
        TextView email = view.findViewById(R.id.alert_detials_email);
        TextView www = view.findViewById(R.id.alert_detials_www);
        Button navigateButton = view.findViewById(R.id.navigate_button);

        message.setText(alert.getMessage());
        centerName.setText(center.getName());
        date.setText(alert.getDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")));

        bloodGroup.setText(String.format("%s%s%s",
                alert.getBlood().getBloodType(),
                " rh ",
                alert.getBlood().getRh())
        );
        address.setText(String.format("%s, %s %s",
                center.getAddress().getStreet(),
                center.getAddress().getPostalCode(),
                center.getAddress().getCity())
        );

        phone.setText(center.getPhoneNumber());
        email.setText(center.getEmail());
        www.setText(center.getWebSite());

        navigateButton.setOnClickListener(v -> {
            locationClient = LocationServices.getFusedLocationProviderClient(getContext());
            locationClient.getLastLocation().addOnCompleteListener(task -> {
                if(task.isSuccessful()) {
                    Location location = task.getResult();

                    String geoUri = "https://www.google.com/maps/dir/?api=1&origin=" +
                            location.getLatitude() + "," + location.getLongitude() +"&destination=" + center.getName();
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                    getContext().startActivity(intent);
                }
            });
        });

        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }
}
