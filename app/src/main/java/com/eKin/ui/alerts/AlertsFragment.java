package com.eKin.ui.alerts;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eKin.R;
import com.eKin.adapters.AlertRecyclerAdapter;
import com.eKin.adapters.OnAlertListener;
import com.eKin.models.Alert;
import com.eKin.models.Center;
import com.eKin.ui.settings.SettingsActivity;
import com.eKin.util.VerticalSpacingItemDecorator;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.util.List;

public class AlertsFragment extends Fragment implements OnAlertListener {
    private FusedLocationProviderClient locationClient;

    private AlertsViewModel alertsViewModel;
    private RecyclerView recyclerView;
    private AlertRecyclerAdapter recyclerAdapter;
    private View view;
    private RadioGroup radioGroup;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        view = inflater.inflate(R.layout.fragment_alerts, container, false);

        alertsViewModel = ViewModelProviders.of(this).get(AlertsViewModel.class);
        radioGroup = view.findViewById(R.id.radio_options);

        initRadioButtonsDisplay();
        setRadioOption();
        radioGroup.setOnCheckedChangeListener(checkedRadioListener);
        recyclerView = view.findViewById(R.id.alerts_list);

        initRecyclerView();
        subscribeObservers();
        loadAlerts();

        return view;
    }

    private void setRadioOption() {
        String alertsType = alertsViewModel.getAlertsType();

        if (alertsType.equals("preferred") && view.findViewById(R.id.radio_obserwowane).isEnabled()) {
            radioGroup.check(R.id.radio_obserwowane);
        } else if (alertsType.equals("proximal") && view.findViewById(R.id.radio_najblizsze).isEnabled()) {
            radioGroup.check(R.id.radio_najblizsze);
        } else {
            radioGroup.check(R.id.radio_wszystkie);
        }
    }

    private void loadAlerts() {
        if(alertsViewModel.getAlertsType().equals("proximal")) {
            getAlertsByProximity();
        } else {
            alertsViewModel.getFirstAlerts();
        }
    }

    private void initRadioButtonsDisplay() {
        if (!alertsViewModel.isFilteringOn()) {
            view.findViewById(R.id.radio_obserwowane).setEnabled(false);
            view.findViewById(R.id.radio_najblizsze).setEnabled(false);
            return;
        }

        if (alertsViewModel.isByPreferenceOn()) {
            view.findViewById(R.id.radio_obserwowane).setEnabled(true);
        } else {
            view.findViewById(R.id.radio_obserwowane).setEnabled(false);
        }

        if (alertsViewModel.isByProximityOn()) {
            view.findViewById(R.id.radio_najblizsze).setEnabled(true);
        } else {
            view.findViewById(R.id.radio_najblizsze).setEnabled(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initRadioButtonsDisplay();
    }

    private RadioGroup.OnCheckedChangeListener checkedRadioListener = (radioGroup, id) -> {
        switch(id) {
            case R.id.radio_wszystkie:
                alertsViewModel.setAlertsType("all");
                break;
            case R.id.radio_obserwowane:
                alertsViewModel.setAlertsType("preferred");
                break;
            case R.id.radio_najblizsze:
                alertsViewModel.setAlertsType("proximal");
                break;
        }

        loadAlerts();
    };

    private void subscribeObservers() {
        alertsViewModel.getAlerts().observe(getViewLifecycleOwner(), new Observer<List<Alert>>() {
            @Override
            public void onChanged(List<Alert> alerts) {
                if (alerts != null && alerts.size() > 0) {
                    recyclerAdapter.setAlerts(alerts);
                } else {
                    recyclerAdapter.showEmptyInfo();
                }
            }
        });

        alertsViewModel.getCenters().observe(getViewLifecycleOwner(), new Observer<List<Center>>() {
            @Override
            public void onChanged(List<Center> centers) {
                if (centers != null) {
                    alertsViewModel.setAvailableCenters(centers);
                    recyclerAdapter.setCenters(centers);
                }
            }
        });

        alertsViewModel.isQueryExhausted().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isExhausted) {
                if (isExhausted) {
                    recyclerAdapter.setQueryExhausted();
                }
            }
        });

        alertsViewModel.isAlertRequestTimeout().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isTimeout) {
                if(isTimeout){
                    recyclerAdapter.setRequestTimeout();
                }
            }
        });

        alertsViewModel.isPerformingQuery().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isPerf) {
                if(isPerf){
                    recyclerAdapter.displayLoading();
                }
            }
        });
    }

    private void initRecyclerView() {
        recyclerAdapter = new AlertRecyclerAdapter(this);

        VerticalSpacingItemDecorator itemDecorator = new VerticalSpacingItemDecorator(50);

        recyclerView.addItemDecoration(itemDecorator);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (!recyclerView.canScrollVertically(1)) {
                    alertsViewModel.getNextAlerts();
                }
            }
        });
    }

    private void getAlertsByProximity() {
        locationClient = LocationServices.getFusedLocationProviderClient(getContext());
        locationClient.getLastLocation().addOnCompleteListener(task -> {
            if(task.isSuccessful()) {
                Location location = task.getResult();
                alertsViewModel.setLastLocation(location);
                alertsViewModel.getFirstAlerts();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_settings, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent preferencesIntent = new Intent(getContext(), SettingsActivity.class);
        startActivity(preferencesIntent);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAlertClick(int position) {
        Alert alert = recyclerAdapter.getSelectedAlert(position);
        Center center = recyclerAdapter.getCenterForAlert(position);

        FragmentManager fm = getChildFragmentManager();
        AlertDetails alertDetails = AlertDetails.newInstance(alert, center);
        alertDetails.show(fm, "alert_details");
    }
}