package com.eKin.ui.map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.eKin.R;
import com.eKin.adapters.NearestCenterInfoWindowAdapter;
import com.eKin.models.Center;
import com.eKin.models.CentersWithResources;
import com.eKin.requests.responses.BloodResourcesResponse;
import com.eKin.service.InfoService;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class MapFragment extends Fragment implements OnMapReadyCallback, SeekBar.OnSeekBarChangeListener {

    private final static double AVERAGE_RADIUS_OF_EARTH_KM = 6371;
    private static final int LOCATION_REQUEST = 500;

    private Context thisContext;
    private GoogleMap mMap;
    private ArrayList<LatLng> listPoints;

    private Button buttonDraw;
    private Button buttonClear;
    private TextView resultOfDistanceCalculation;

    private List<Marker> twoMarkersList = new ArrayList<>();
    private Polyline polyline = null;
    private LatLng aPointLatLng;
    private LatLng bPointLatLng;


    private String nearestCenterFromUser = "";

    private Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://ekrewni.pl/ekrewni/")
            //.baseUrl("http://10.0.2.2:8080/ekrewni/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

    private InfoService infoService = retrofit.create(InfoService.class);


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.map_fragment, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);

        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }


        Button buttonShowNearestCenter = view.findViewById(R.id.bt_show_nearest_center);
        buttonShowNearestCenter.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                calculateNearestCenter();
            }
        });

        listPoints = new ArrayList<>();
        buttonDraw = view.findViewById(R.id.bt_draw);
        buttonClear = view.findViewById(R.id.bt_clear);

        buttonDraw.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (polyline != null) {
                    polyline.remove();
                }

                drawPolyline(aPointLatLng, bPointLatLng, resultOfDistanceCalculation);
            }
        });

        resultOfDistanceCalculation = (TextView) view.findViewById(R.id.distanceCalculate);

        buttonClear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (polyline != null) {
                    polyline.remove();
                    for (Marker marker : twoMarkersList) {
                        marker.remove();
                        listPoints.clear();
                        resultOfDistanceCalculation.setText("");
                        buttonClear.setEnabled(false);
                        buttonDraw.setEnabled(false);

                    }
                }
            }
        });

        return view;
    }

    private void drawPolyline(LatLng pointA, LatLng pointB, TextView textView) {

        PolylineOptions polylineOptions = new PolylineOptions().addAll(listPoints).clickable(true);
        this.aPointLatLng = pointA;
        this.bPointLatLng = pointB;
        this.resultOfDistanceCalculation = textView;

        aPointLatLng = listPoints.get(0);
        bPointLatLng = listPoints.get(1);

        double latA = aPointLatLng.latitude;
        double lonA = aPointLatLng.longitude;

        double latB = bPointLatLng.latitude;
        double lonB = bPointLatLng.longitude;

        int distance = calculateDistanceInKilometer(latA, lonA, latB, lonB);

        resultOfDistanceCalculation.setText(String.valueOf(distance) + " km");

        polyline = mMap.addPolyline(polylineOptions);
        polyline.setColor(Color.GREEN);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        markCentersOnMap(mMap);


        if (ActivityCompat.checkSelfPermission(thisContext,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(thisContext,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);


            mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {

                @Override
                public void onMapLongClick(LatLng latLng) {


                    if (listPoints.size() == 2) {
                        listPoints.clear();

                        if(polyline != null) {
                            polyline.remove();
                        }

                        for (Marker marker: twoMarkersList) {
                            marker.remove();
                        }
                        buttonDraw.setEnabled(false);
                        buttonClear.setEnabled(false);
                    }

                    listPoints.add(latLng);
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);


                    if (listPoints.size() == 1) {
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
                        markerOptions.title(" Punkt A ");
                    } else {
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
                        markerOptions.title(" Punkt B ");
                        buttonDraw.setEnabled(true);
                        buttonClear.setEnabled(true);
                    }
                    Marker marker = mMap.addMarker(markerOptions);
                    twoMarkersList.add(marker);

                }
            });

        }
    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        thisContext = context;
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case LOCATION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);
                }
                break;
        }
    }

    private void markCentersOnMap(GoogleMap mMap) {

        Call<List<CentersWithResources>> call = infoService.getAllCentersWithBloodResources();
        call.enqueue(new Callback<List<CentersWithResources>>() {

            @Override
            public void onResponse(Call<List<CentersWithResources>> call, Response<List<CentersWithResources>> response) {

                if (!response.isSuccessful()) {
                    return;
                }

                List<CentersWithResources> centersWithResource = response.body();
                List<CentersWithResources> finalListCenntersWithResources = new ArrayList<>();
                HashMap<String, CentersWithResources> hashListCentersWithResources = new HashMap<>();

                for (int i = 0; i < centersWithResource.size(); i++) {

                    CentersWithResources centerAndResources = centersWithResource.get(i);
                    String centerId = centerAndResources.getCenterId();
                    hashListCentersWithResources.put(centerId, centerAndResources);

                }

                Collection<CentersWithResources> values = hashListCentersWithResources.values();
                ArrayList<CentersWithResources> listOfValues = new ArrayList<CentersWithResources>(values);

                // Lista centrów z zasobami
                finalListCenntersWithResources.addAll(listOfValues);

                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                mMap.getUiSettings();
                mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
                String name = "";
                String street = "";
                String postalCode = "";
                String city = "";
                String phone = "";
                String email = "";
                String www = "";

                String bloodType = "";
                String rh = "";
                double volume = 0;

                String snippet = "";
                double latitude;
                double longitude;
                List<BloodResourcesResponse> listOfResourcesForEachCenter = new ArrayList<>();


                for (CentersWithResources center : finalListCenntersWithResources) {

                    name = center.getName();
                    street = center.getAddress().getStreet();
                    postalCode = center.getAddress().getPostalCode();
                    city = center.getAddress().getCity();
                    phone = center.getPhoneNumber();
                    email = center.getEmail();
                    www = center.getWebSite();
                    listOfResourcesForEachCenter = center.getListOfBloodResources();


                    snippet = "Adres: ".toUpperCase() + "\n" + "ul. " + street + "\n" + postalCode + " " + city + "\n" + "tel. " + phone + "\n" + "email: " + email + "\n" + "www: " + www
                            + "\n\n" + "Stany magazynowe krwi: ".toUpperCase() + listOfResourcesForEachCenter;

                    longitude = Double.parseDouble(center.getAddress().getLongitude());
                    latitude = Double.parseDouble(center.getAddress().getLatitude());
                    LatLng cityCoordinates = new LatLng(latitude, longitude);


                    double bloodVolume0 = listOfResourcesForEachCenter.get(0).getVolume();
                    double bloodVolume1 = listOfResourcesForEachCenter.get(1).getVolume();
                    double bloodVolume2 = listOfResourcesForEachCenter.get(2).getVolume();
                    double bloodVolume3 = listOfResourcesForEachCenter.get(3).getVolume();
                    double bloodVolume4 = listOfResourcesForEachCenter.get(4).getVolume();
                    double bloodVolume5 = listOfResourcesForEachCenter.get(5).getVolume();
                    double bloodVolume6 = listOfResourcesForEachCenter.get(6).getVolume();
                    double bloodVolume7 = listOfResourcesForEachCenter.get(7).getVolume();

                    if ((bloodVolume0 >= 0 && bloodVolume0 < 500) || (bloodVolume1 >= 0 && bloodVolume1 < 500) || (bloodVolume2 >= 0 && bloodVolume2 < 500) || (bloodVolume3 >= 0 && bloodVolume3 < 500) || (bloodVolume4 >= 0 && bloodVolume4 < 500) || (bloodVolume5 >= 0 && bloodVolume5 < 500) || (bloodVolume6 >= 0 && bloodVolume6 < 500) || (bloodVolume7 >= 0 && bloodVolume7 < 500)) {

                        mMap.addMarker(new MarkerOptions().position(cityCoordinates).title(name).snippet(snippet).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                        mMap.setInfoWindowAdapter(new NearestCenterInfoWindowAdapter(thisContext));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(cityCoordinates));
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cityCoordinates, 5));

                    } else if ((bloodVolume0 >= 500 && bloodVolume0 < 1000) && (bloodVolume1 >= 500 && bloodVolume1 < 1000) && (bloodVolume2 >= 500 && bloodVolume2 < 1000) && (bloodVolume3 >= 500 && bloodVolume3 < 1000) && (bloodVolume4 >= 500 && bloodVolume4 < 1000) && (bloodVolume5 >= 500 && bloodVolume5 < 1000) && (bloodVolume6 >= 500 && bloodVolume6 < 1000) && (bloodVolume7 >= 500 && bloodVolume7 < 1000)) {

                        mMap.addMarker(new MarkerOptions().position(cityCoordinates).title(name).snippet(snippet).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                        mMap.setInfoWindowAdapter(new NearestCenterInfoWindowAdapter(thisContext));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(cityCoordinates));
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cityCoordinates, 5));

                    } else if ((bloodVolume0 >= 1000) && (bloodVolume1 >= 1000) && (bloodVolume2 >= 1000) && (bloodVolume3 >= 1000) && (bloodVolume4 >= 1000) && (bloodVolume5 >= 1000) && (bloodVolume6 >= 1000) && (bloodVolume7 >= 1000)) {
                        mMap.addMarker(new MarkerOptions().position(cityCoordinates).title(name).snippet(snippet).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                        mMap.setInfoWindowAdapter(new NearestCenterInfoWindowAdapter(thisContext));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(cityCoordinates));
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cityCoordinates, 5));
                    } else {

                        mMap.addMarker(new MarkerOptions().position(cityCoordinates).title(name).snippet(snippet).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                        mMap.setInfoWindowAdapter(new NearestCenterInfoWindowAdapter(thisContext));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(cityCoordinates));
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cityCoordinates, 5));

                    }
                }

            }

            @Override
            public void onFailure(Call<List<CentersWithResources>> call, Throwable t) {

            }
        });
    }

    private LatLng getUserLocation() {

        LocationManager lm = (LocationManager) thisContext.getSystemService(thisContext.LOCATION_SERVICE);
        assert lm != null;
        @SuppressLint("MissingPermission") Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        double longitude = location.getLongitude();
        double latitude = location.getLatitude();

        return new LatLng(latitude, longitude);
    }

    private void calculateNearestCenter() {
        LatLng userLocation = getUserLocation();
        double userLocationLat = userLocation.latitude;
        double userLocationLon = userLocation.longitude;

        Call<List<Center>> call = infoService.getAllCenters();
        call.enqueue(new Callback<List<Center>>() {

            @Override
            public void onResponse(Call<List<Center>> call, Response<List<Center>> response) {

                if (!response.isSuccessful()) {
                    return;
                }

                List<Center> centerList = response.body();

                String name = "";
                double latitude;
                double longitude;
                double distance = 0;

                HashMap<Double, String> distanceBetweenUserAndCenter = new HashMap<Double, String>();


                for (Center center : centerList) {
                    name = center.getName();
                    String centerLongitude = center.getAddress().getLongitude();
                    String centerLatitude = center.getAddress().getLatitude();
                    longitude = Double.parseDouble(centerLongitude);
                    latitude = Double.parseDouble(centerLatitude);
                    distance = calculateDistanceInKilometer(userLocationLat, userLocationLon, latitude, longitude);
                    distanceBetweenUserAndCenter.put(distance, name);
                }


                Double minValue = 50000.0;

                for (Map.Entry<Double, String> entry : distanceBetweenUserAndCenter.entrySet()) {
                    Double centerValue = entry.getKey();
                    if (centerValue <= minValue) {
                        minValue = centerValue;
                    }
                }

                nearestCenterFromUser = distanceBetweenUserAndCenter.get(minValue);
                for (Center center : centerList) {
                    if ((center.getName()).equals(nearestCenterFromUser)) {
                        double centLat = Double.parseDouble(center.getAddress().getLatitude());
                        double centLon = Double.parseDouble(center.getAddress().getLongitude());
                        LatLng centPos = new LatLng(centLat, centLon);

                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
                        markerOptions.position(centPos);
                        markerOptions.title(center.getName());
                        mMap.addMarker(markerOptions);
                    }
                }

            }

            @Override
            public void onFailure(Call<List<Center>> call, Throwable t) {

            }
        });

    }

    private int calculateDistanceInKilometer(double userLat, double userLng,
                                             double venueLat, double venueLng) {

        double latDistance = Math.toRadians(userLat - venueLat);
        double lngDistance = Math.toRadians(userLng - venueLng);

        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(userLat)) * Math.cos(Math.toRadians(venueLat))
                * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return (int) (Math.round(AVERAGE_RADIUS_OF_EARTH_KM * c));
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}