package com.eKin.repositories;

import com.eKin.models.Alert;
import com.eKin.requests.ApiClient;
import java.util.List;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

public class AlertRepository {
    private static AlertRepository instance;
    private ApiClient apiClient;

    private int pageNumber;
    private MutableLiveData<Boolean> isQueryExhausted = new MutableLiveData<>();
    private MediatorLiveData<List<Alert>> alerts = new MediatorLiveData<>();

    public static AlertRepository getInstance() {
        if (instance == null) {
            instance = new AlertRepository();
        }

        return instance;
    }

    private AlertRepository() {
        apiClient = ApiClient.getInstance();
        initMediators();
    }

    private void initMediators() {
        LiveData<List<Alert>> alertsListApiSource = apiClient.getAlerts();

        alerts.addSource(alertsListApiSource, newAlerts -> {
            if (newAlerts != null) {
                alerts.setValue(newAlerts);
                doneQuery(newAlerts);
            } else {
                doneQuery(null);
            }
        });
    }

    private void doneQuery(List<Alert> list) {
        if (list != null) {
             if(list.size() % 10 != 0) {
                 isQueryExhausted.setValue(true);
             }
        } else {
            isQueryExhausted.setValue(true);
        }
    }

    public LiveData<Boolean> isQueryExhausted() {
        return isQueryExhausted;
    }

    public LiveData<List<Alert>> getAlerts() {
        return alerts;
    }

    private void searchAlertsApi(int pageNumber, List<String> centers, List<String> groups){
        this.pageNumber = pageNumber;
        isQueryExhausted.setValue(false);
        apiClient.getAlertsFromApi(pageNumber, groups, centers);
    }


    public void getFirstPage(List<String> centers, List<String> groups) {
        searchAlertsApi(0, centers, groups);
    }

    public void getNextPage(List<String> centers, List<String> groups) {
        searchAlertsApi(pageNumber + 1, centers, groups);
    }


    public void cancelRequest() {
        apiClient.cancelRequest();
    }

    public LiveData<Boolean> isRequestTimedOut() {
        return apiClient.isAlertsRequestTimeout();
    }
}
