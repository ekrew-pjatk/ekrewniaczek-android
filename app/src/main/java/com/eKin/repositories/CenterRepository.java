package com.eKin.repositories;

import android.util.Log;

import com.eKin.models.Center;
import com.eKin.requests.ApiClient;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

import static android.content.ContentValues.TAG;

public class CenterRepository {
    private ApiClient apiClient;
    private static CenterRepository instance;
    private MediatorLiveData<List<Center>> centers = new MediatorLiveData<>();

    public static CenterRepository getInstance() {
        if (instance == null) {
            instance = new CenterRepository();
        }

        return instance;
    }

    private CenterRepository() {
        apiClient = ApiClient.getInstance();
        initMediators();
    }

    private void initMediators() {
        LiveData<List<Center>> alertsListApiSource = apiClient.getCenters();

        centers.addSource(alertsListApiSource, new Observer<List<Center>>() {
            @Override
            public void onChanged(@Nullable List<Center> centersList) {
                if (centers != null) {
                    centers.setValue(centersList);
                }
            }
        });
    }

    public void fetchCenters() {
        apiClient.getCentersFromApi();
    }

    public LiveData<List<Center>> getCenters() {
        Log.d(TAG, "getAlerts:" + centers.toString());
        return centers;
    }
}
