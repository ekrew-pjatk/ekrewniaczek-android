package com.eKin.repositories;

import android.util.Log;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;
import com.eKin.models.Blood;
import com.eKin.requests.ApiClient;
import java.util.List;
import static android.content.ContentValues.TAG;

public class BloodRepository {

    private ApiClient apiClient;
    private static BloodRepository instance;
    private MediatorLiveData<List<Blood>> bloodGroups = new MediatorLiveData<>();

    public static BloodRepository getInstance() {

        if (instance == null) {

            instance = new BloodRepository();
        }
        return instance;
    }

    private BloodRepository() {

        apiClient = ApiClient.getInstance();
        initMediators();
    }

    private void initMediators() {

        LiveData<List<Blood>> bloodListApiSource = apiClient.getBloodGroups();
        bloodGroups.addSource(bloodListApiSource, new Observer<List<Blood>>() {

            @Override
            public void onChanged(@Nullable List<Blood> bloodList) {

                if (bloodGroups != null) {

                    bloodGroups.setValue(bloodList);
                }
            }
        });
    }

    public void getBloodGroupsFromApi() {

        apiClient.getBloodGroupsFromApi();
    }

    public LiveData<List<Blood>> getBloodGroups() {

        Log.d(TAG, "getBloodGroups:" + bloodGroups.toString());
        return bloodGroups;
    }
}