package com.eKin.requests;

import com.eKin.service.AlertsService;
import com.eKin.service.BloodService;
import com.eKin.service.CentersService;
import com.eKin.util.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class ServiceGenerator {
    private static Retrofit.Builder retrofitBuilder =
            new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = retrofitBuilder.build();

    private static AlertsService alertsService = retrofit.create(AlertsService.class);
    private static CentersService centersService = retrofit.create(CentersService.class);
    private static BloodService bloodService = retrofit.create(BloodService.class);

    static AlertsService getAlertsService() {
        return alertsService;
    }

    static CentersService getCentersService() {
        return centersService;
    }

    static BloodService getBloodService() {
        return bloodService;
    }
}