package com.eKin.requests.mappers;

public interface Mapper<From, To> {
    To map(From from);
}