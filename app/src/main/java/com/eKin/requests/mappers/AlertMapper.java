package com.eKin.requests.mappers;

import com.eKin.models.Alert;
import com.eKin.requests.responses.AlertResponse;

import java.time.LocalDateTime;

public class AlertMapper implements Mapper<AlertResponse, Alert> {

    @Override
    public Alert map(AlertResponse response) {
        Alert alert = new Alert();

        alert.setAlertId(response.getAlertId());
        alert.setMessage(response.getMessage());
        alert.setCenterId(response.getCenterId());
        alert.setBlood(response.getBlood());
        alert.setDate(LocalDateTime.parse(response.getDate()));

        return alert;
    }
}
