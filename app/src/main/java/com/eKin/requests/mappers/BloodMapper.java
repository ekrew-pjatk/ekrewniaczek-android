package com.eKin.requests.mappers;

import com.eKin.models.Blood;
import com.eKin.requests.responses.BloodResponse;

public class BloodMapper implements Mapper<BloodResponse, Blood> {
    @Override
    public Blood map(BloodResponse bloodResponse) {
        Blood blood = new Blood();

        blood.setBloodId(bloodResponse.getBloodId());
        blood.setBloodType(bloodResponse.getBloodType());
        blood.setRh(bloodResponse.getRh());
        return blood;
    }
}
