package com.eKin.requests.mappers;

import com.eKin.models.Center;
import com.eKin.requests.responses.CenterResponse;

public class CenterMapper implements Mapper<CenterResponse, Center> {

    @Override
    public Center map(CenterResponse response) {
        Center center = new Center();

        center.setAddress(response.getAddress());
        center.setCenterId(response.getCenterId());
        center.setEmail(response.getEmail());
        center.setName(response.getName());
        center.setPhoneNumber(response.getPhoneNumber());
        center.setWebSite(response.getWebSite());

        return center;
    }
}
