package com.eKin.requests;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.eKin.AppExecutors;
import com.eKin.models.Alert;
import com.eKin.models.Blood;
import com.eKin.models.Center;
import com.eKin.requests.mappers.AlertMapper;
import com.eKin.requests.mappers.BloodMapper;
import com.eKin.requests.mappers.CenterMapper;
import com.eKin.requests.responses.AlertResponse;
import com.eKin.requests.responses.BloodResponse;
import com.eKin.requests.responses.CenterResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Response;

import static com.eKin.util.Constants.NETWORK_TIMEOUT;

public class ApiClient {
    private static final String TAG = "ApiClient";
    private static ApiClient instance;

    private AlertsRunnable alertsRunnable;
    private CentersRunnable centersRunnable;
    private BloodRunnable bloodRunnable;

    private MutableLiveData<List<Alert>> alerts;
    private MutableLiveData<List<Center>> centers;
    private MutableLiveData<List<Blood>> bloodGroups;
    private MutableLiveData<Boolean> alertsRequestTimeout = new MutableLiveData<>();

    public static ApiClient getInstance() {
        if(instance == null) {
            instance = new ApiClient();
        }
        return instance;
    }

    private ApiClient(){
        alerts = new MutableLiveData<>();
        centers = new MutableLiveData<>();
        bloodGroups = new MutableLiveData<>();
    }

    public LiveData<List<Alert>> getAlerts() {
        return alerts;
    }

    public LiveData<List<Center>> getCenters() {
        return centers;
    }

    public LiveData<Boolean> isAlertsRequestTimeout() {
        return alertsRequestTimeout;
    }

    public void getAlertsFromApi(int pageNumber, List<String> groups, List<String> centers) {
        if(alertsRunnable != null){
            alertsRunnable = null;
        }

        alertsRunnable = new AlertsRunnable(pageNumber, groups, centers);
        final Future handler = AppExecutors.getInstance().alertsIO().submit(alertsRunnable);

        alertsRequestTimeout.setValue(false);
        AppExecutors.getInstance().alertsIO().schedule(new Runnable() {
            @Override
            public void run() {
                alertsRequestTimeout.setValue(true);
                // let the user know its timed out
                handler.cancel(true);
            }
        }, NETWORK_TIMEOUT, TimeUnit.MILLISECONDS);
    }

    public void getCentersFromApi() {
        if(centersRunnable != null){
            centersRunnable = null;
        }

        centersRunnable = new CentersRunnable();
        final Future handler = AppExecutors.getInstance().alertsIO().submit(centersRunnable);

        alertsRequestTimeout.setValue(false);
        AppExecutors.getInstance().alertsIO().schedule(new Runnable() {
            @Override
            public void run() {
                alertsRequestTimeout.setValue(true);
                // let the user know its timed out
                handler.cancel(true);
            }
        }, NETWORK_TIMEOUT, TimeUnit.MILLISECONDS);
    }

    public void getBloodGroupsFromApi() {
        if (bloodRunnable != null) {
            bloodRunnable = null;
        }

        bloodRunnable = new BloodRunnable();
        final Future handler = AppExecutors.getInstance().alertsIO().submit(bloodRunnable);

        AppExecutors.getInstance().alertsIO().schedule(new Runnable() {
            @Override
            public void run() {
                // let the user know its timed out
                handler.cancel(true);
            }
        }, NETWORK_TIMEOUT, TimeUnit.MILLISECONDS);
    }

    public LiveData<List<Blood>> getBloodGroups() {

        return bloodGroups;
    }

    private class AlertsRunnable implements Runnable {
        private int pageNumber;
        private List<String> groups;
        private List<String> centers;

        boolean cancelRequest;

        AlertsRunnable(int pageNumber, List<String> groups, List<String> centers) {
            this.pageNumber = pageNumber;
            this.groups = groups;
            this.centers = centers;
            cancelRequest = false;
        }

        @Override
        public void run() {
            try {
                Response<List<AlertResponse>> response = getAlerts(pageNumber, groups, centers).execute();
                Log.d(TAG, "alerts: "+ response);

                if(cancelRequest) {
                    return;
                }

                if(response.code() == 200) {
                    List<Alert> alertsList = new ArrayList<>();
                    List<AlertResponse> alertsResponseList = response.body();

                    if (alertsResponseList != null) {
                        for (AlertResponse alertResponse: alertsResponseList) {
                            alertsList.add(new AlertMapper().map(alertResponse));
                        }
                    }

                    if(pageNumber == 0) {
                        alerts.postValue(alertsList);
                    } else {
                        List<Alert> currentAlerts = alerts.getValue();
                        currentAlerts.addAll(alertsList);
                        alerts.postValue(currentAlerts);
                    }
                }
                else {
                    String error = response.errorBody().string();
                    Log.e(TAG, "run: " + error );
                    alerts.postValue(null);
                }
            } catch (IOException e) {
                e.printStackTrace();
                alerts.postValue(null);
                alertsRequestTimeout.setValue(true);
            }

        }

        private Call<List<AlertResponse>> getAlerts(int pageNumber, List<String> groups, List<String> centers){
            return ServiceGenerator.getAlertsService().getAlerts(pageNumber, groups, centers);
        }

        private void cancelRequest(){
            Log.d(TAG, "cancelRequest: canceling the search request.");
            cancelRequest = true;
        }
    }

    public void cancelRequest(){
        if(alertsRunnable != null){
            alertsRunnable.cancelRequest();
        }
    }

    private class BloodRunnable implements Runnable {
        boolean cancelRequest;

        BloodRunnable() {
            cancelRequest = false;
        }

        @Override
        public void run() {
            try {
                Response<List<BloodResponse>> response = getBloodGroups().execute();
                Log.d(TAG, "centesr: " + response);

                if (cancelRequest) {
                    return;
                }

                if (response.code() == 200) {
                    List<Blood> newBloodGroups = new ArrayList<>();
                    List<BloodResponse> list = response.body();

                    if (list != null) {
                        for (BloodResponse bloodResponse : list) {
                            newBloodGroups.add(new BloodMapper().map(bloodResponse));
                        }
                    }

                    bloodGroups.postValue(newBloodGroups);
                } else {
                    String error = response.errorBody().string();
                    Log.e(TAG, "run: " + error);
                    bloodGroups.postValue(null);
                }
            } catch (IOException e) {
                e.printStackTrace();
                bloodGroups.postValue(null);
            }

        }

        private Call<List<BloodResponse>> getBloodGroups() {
            return ServiceGenerator.getBloodService().getBloodGroups();
        }

        private void cancelRequest() {
            Log.d(TAG, "cancelRequest: canceling the search request.");
            cancelRequest = true;
        }
    }

    private class CentersRunnable implements Runnable {
        boolean cancelRequest;

        CentersRunnable() {
            cancelRequest = false;
        }

        @Override
        public void run() {
            try {
                Response<List<CenterResponse>> response = getCenters().execute();
                Log.d(TAG, "centesr: " + response);

                if(cancelRequest) {
                    return;
                }

                if(response.code() == 200){
                    List<Center> newCenters = new ArrayList<>();
                    List<CenterResponse> list = response.body();

                    if (list != null) {
                        for (CenterResponse centerResponse: list) {
                            newCenters.add(new CenterMapper().map(centerResponse));
                        }
                    }

                    centers.postValue(newCenters);
                }
                else {
                    String error = response.errorBody().string();
                    Log.e(TAG, "run: " + error );
                    centers.postValue(null);
                }
            } catch (IOException e) {
                e.printStackTrace();
                centers.postValue(null);
            }

        }

        private Call<List<CenterResponse>> getCenters(){
            return ServiceGenerator.getCentersService().getCenters();
        }

        private void cancelRequest(){
            Log.d(TAG, "cancelRequest: canceling the search request.");
            cancelRequest = true;
        }
    }
}
