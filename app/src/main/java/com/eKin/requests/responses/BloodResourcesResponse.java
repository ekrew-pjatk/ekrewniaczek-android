package com.eKin.requests.responses;

import androidx.annotation.NonNull;

import com.eKin.models.Blood;

public class BloodResourcesResponse {

    private String bloodResourceId;
    private double volume;
    private Blood blood;
    private String centerId;

    public String getBloodResourceId() {

        return bloodResourceId;
    }

    public void setBloodResourceId(String bloodResourceId) {

        this.bloodResourceId = bloodResourceId;
    }

    public double getVolume() {

        return volume;
    }

    public void setVolume(double volume) {

        this.volume = volume;
    }

    public Blood getBlood() {

        return blood;
    }

    public void setBlood(Blood blood) {

        this.blood = blood;
    }

    public String getCenterId() {

        return centerId;
    }

    public void setCenterId(String centerId) {

        this.centerId = centerId;
    }

    @NonNull
    @Override
    public String toString() {

        return new StringBuilder()
                .append("\n" + getBlood())
                .append(" " + (int) volume + " litrów")
                .toString();
    }
}
