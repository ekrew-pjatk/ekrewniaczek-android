package com.eKin.requests.responses;

import com.eKin.models.Blood;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AlertResponse {
    private String alertId;
    private String message;
    private String centerId;
    private String date;

    @SerializedName("blood")
    @Expose
    private Blood blood;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAlertId() {
        return alertId;
    }

    public void setAlertId(String alertId) {
        this.alertId = alertId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

    public Blood getBlood() {
        return blood;
    }

    public void setBlood(Blood blood) {
        this.blood = blood;
    }
}
