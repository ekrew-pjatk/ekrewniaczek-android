package com.eKin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.eKin.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class NearestCenterInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private final View nearestCenterWindow;
    private Context thisContext;

    public NearestCenterInfoWindowAdapter(Context thisContext) {

        this.thisContext = thisContext;
        nearestCenterWindow = LayoutInflater.from(thisContext).inflate(R.layout.nearest_center_info_window, null);
    }

    private void renderWindowtext(Marker marker, View view) {

        String title = marker.getTitle();
        String snippet = marker.getSnippet();
        TextView markerTitle = (TextView) view.findViewById(R.id.marker_title);
        TextView markerSnippet = (TextView) view.findViewById(R.id.marker_snippet);
        if (!title.equals("")) {
            markerTitle.setText(title);
            markerSnippet.setText(snippet);
        }

    }

    @Override
    public View getInfoWindow(Marker marker) {

        renderWindowtext(marker, nearestCenterWindow);

        return nearestCenterWindow;
    }

    @Override
    public View getInfoContents(Marker marker) {

        renderWindowtext(marker, nearestCenterWindow);

        return nearestCenterWindow;
    }
}
