package com.eKin.adapters;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

class ListEmptyViewHolder extends RecyclerView.ViewHolder {
    ListEmptyViewHolder(@NonNull View itemView) {
        super(itemView);
    }
}
