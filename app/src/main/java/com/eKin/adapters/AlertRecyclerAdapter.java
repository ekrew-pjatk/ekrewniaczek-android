package com.eKin.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eKin.R;
import com.eKin.models.Alert;
import com.eKin.models.Center;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class AlertRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int ALERT_TYPE = 1;
    private static final int EXHAUSTED_TYPE = 2;
    private static final int LOADING_TYPE = 3;
    private static final int EMPTY_TYPE = 4;
    private static final int TIMEOUT = 5;

    private List<Alert> alerts;
    private List<Center> centers;

    private OnAlertListener onAlertListener;

    public AlertRecyclerAdapter(OnAlertListener onAlertListener) {
        this.onAlertListener = onAlertListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case LOADING_TYPE:{
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.loading_list_item, parent, false);
                return new LoadingViewHolder(view);
            }
            case EMPTY_TYPE:
            case TIMEOUT:{
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.alert_empty, parent, false);
                return new ListExhaustedViewHolder(view);
            }
            case EXHAUSTED_TYPE: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.alert_exhausted, parent, false);
                return new ListEmptyViewHolder(view);
            }
            case ALERT_TYPE:
            default: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.alert_list_item, parent, false);
                return new AlertViewHolder(view, onAlertListener);
            }
        }


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int itemViewType = getItemViewType(position);

        if (itemViewType == ALERT_TYPE) {
            String centerId = alerts.get(position).getCenterId();
            Center center = centers.stream().filter(c -> centerId.equals(c.getCenterId())).findAny().orElse(null);

            ((AlertViewHolder) holder).center.setText(center != null ? center.getAddress().getCity() : "Nieznane centrum");
            ((AlertViewHolder) holder).date.setText(alerts.get(position).getDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));;
            ((AlertViewHolder) holder).group.setText(String.format("%s%s%s",
                    "Potrzebna grupa: ",
                    alerts.get(position).getBlood().getBloodType(),
                    alerts.get(position).getBlood().getRh())
            );
        }
    }

    @Override
    public int getItemCount() {
        if (alerts != null) {
            return alerts.size();
        }

        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        switch (alerts.get(position).getMessage()) {
            case "EXHAUSTED":
                return EXHAUSTED_TYPE;
            case "LOADING":
                return LOADING_TYPE;
            case "EMPTY":
                return EMPTY_TYPE;
            case "TIMEOUT":
                return TIMEOUT;
        }

        return ALERT_TYPE;
    }

    public void displayLoading(){
        if(!isLoading()){
            hideEmpty();

            Alert loading = new Alert();
            loading.setMessage("LOADING");
            List<Alert> loadingList = new ArrayList<>();
            loadingList.add(loading);
            alerts = loadingList;
            notifyDataSetChanged();
        }
    }

    public void displayEmpty(){
        if(!isLoading()){
            hideEmpty();

            Alert loading = new Alert();
            loading.setMessage("EMPTY");
            List<Alert> loadingList = new ArrayList<>();
            loadingList.add(loading);
            alerts = loadingList;
            notifyDataSetChanged();
        }
    }

    public void setCenters(List<Center> newCenters) {
        centers = newCenters;
    }

    public void setAlerts(List<Alert> newAlerts) {
        alerts = newAlerts;

        if(centers != null) {
            notifyDataSetChanged();
        }
    }

    public Alert getSelectedAlert(int position) {
        if (alerts != null && alerts.size() > 0) {
            return alerts.get(position);
        }

        return null;
    }

    public Center getCenterForAlert(int position) {
        if (alerts != null && alerts.size() > 0) {

            String centerId = alerts.get(position).getCenterId();
            return centers.stream().filter(c -> centerId.equals(c.getCenterId())).findAny().orElse(null);
        }

        return null;
    }

    public void setQueryExhausted() {
        hideLoading();

        Alert exhaustedAlert = new Alert();
        exhaustedAlert.setMessage("EXHAUSTED");
        alerts.add(exhaustedAlert);
        notifyDataSetChanged();
    }

    public void setRequestTimeout() {
        hideLoading();

        Alert timeout = new Alert();
        timeout.setMessage("TIMEOUT");

        List<Alert> emptyList = new ArrayList<>();
        emptyList.add(timeout);
        alerts.add(timeout);
        notifyDataSetChanged();
    }

    public void showEmptyInfo() {
        if(isEmpty()) {
            return;
        }

        hideLoading();
        Alert alert = new Alert();
        alert.setMessage("EMPTY");

        List<Alert> emptyList = new ArrayList<>();
        emptyList.add(alert);
        alerts = emptyList;
        notifyDataSetChanged();
    }

    private void hideLoading() {
        if (isLoading()) {
            for (Alert alert : alerts) {
                if (alert.getMessage().equals("LOADING")) {
                    alerts.remove(alert);
                }
            }

            notifyDataSetChanged();
        }
    }

    private void hideEmpty() {
        if (alerts == null) {
            return;
        }

        Alert empty = alerts.get(0);
        if (empty.getMessage().equals("EMPTY")) {
            alerts.remove(empty);
        }
    }

    private boolean isLoading() {
        if (alerts != null && alerts.size() > 0) {
            return alerts.get(alerts.size() - 1).getMessage().equals("LOADING");
        }

        return false;
    }

    private boolean isEmpty() {
        if (alerts != null && alerts.size() > 0) {
            return alerts.get(alerts.size() - 1).getMessage().equals("EMPTY");
        }

        return false;
    }
}
