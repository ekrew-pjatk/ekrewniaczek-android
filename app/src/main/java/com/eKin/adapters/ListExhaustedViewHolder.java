package com.eKin.adapters;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

class ListExhaustedViewHolder extends RecyclerView.ViewHolder {
    ListExhaustedViewHolder(@NonNull View itemView) {
        super(itemView);
    }
}
