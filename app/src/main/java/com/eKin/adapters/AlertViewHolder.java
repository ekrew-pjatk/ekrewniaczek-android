package com.eKin.adapters;

import com.eKin.R;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AlertViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView center;
    TextView group;
    TextView date;

    private OnAlertListener onAlertListener;

    AlertViewHolder(@NonNull View itemView, OnAlertListener onAlertListener) {
        super(itemView);
        this.onAlertListener = onAlertListener;

        center = itemView.findViewById(R.id.alert_center_shortname);
        date = itemView.findViewById(R.id.alert_date);
        group = itemView.findViewById(R.id.alert_blood);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        onAlertListener.onAlertClick(getAdapterPosition());
    }
}
