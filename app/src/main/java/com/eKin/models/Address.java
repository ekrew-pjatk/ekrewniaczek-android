package com.eKin.models;

public class Address {

    private String addressId;
    private String city;
    private String postalCode;
    private String street;


    private String longitude;
    private String latitude;

    public Address(String addressId, String city, String postalCode, String street, String longitude, String latitude) {
        this.addressId = addressId;
        this.city = city;
        this.postalCode = postalCode;
        this.street = street;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Address() {
    }

    public String getAddressId() {

        return addressId;
    }

    public void setAddressId(String addressId) {

        this.addressId = addressId;
    }

    public String getCity() {

        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
