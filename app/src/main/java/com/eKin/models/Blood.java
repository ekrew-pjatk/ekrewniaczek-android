package com.eKin.models;

import androidx.annotation.NonNull;
import com.google.gson.annotations.SerializedName;

public class Blood {

    @SerializedName("bloodId")
    private String bloodId;
    @SerializedName("bloodType")
    private String bloodType;
    @SerializedName("rh")
    private String rh;

    public Blood(String bloodType, String rh) {

        this.bloodId = bloodId;
        this.bloodType = bloodType;
        this.rh = rh;
    }

    public Blood() {

    }

    public String getBloodType() {

        return bloodType;
    }

    public String getBloodId() {

        return bloodId;
    }

    public void setBloodId(String bloodId) {

        this.bloodId = bloodId;
    }

    public String getRh() {

        return rh;
    }

    public void setBloodType(String bloodType) {

        this.bloodType = bloodType;
    }

    public void setRh(String rh) {

        this.rh = rh;
    }

    @NonNull
    @Override
    public String toString() {

        return new StringBuilder()
                .append(getBloodType())
                .append(" rh " + getRh() + " : ")
                .toString();
    }
}
