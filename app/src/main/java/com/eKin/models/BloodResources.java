package com.eKin.models;

public class BloodResources {

    private String bloodResourceId;
    private double volume;
    private Blood blood;
    private String centerId;

    public BloodResources() {

    }

    public BloodResources(String bloodResourceId, double volume, Blood blood, String centerId) {
        this.bloodResourceId = bloodResourceId;
        this.volume = volume;
        this.blood = blood;
        this.centerId = centerId;
    }

    public String getBloodResourceId() {
        return bloodResourceId;
    }

    public void setBloodResourceId(String bloodResourceId) {
        this.bloodResourceId = bloodResourceId;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public Blood getBlood() {
        return blood;
    }

    public void setBlood(Blood blood) {
        this.blood = blood;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }
}
