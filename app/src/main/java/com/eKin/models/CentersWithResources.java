package com.eKin.models;

import com.eKin.requests.responses.BloodResourcesResponse;

import java.util.List;

public class CentersWithResources {
    private String centerId;
    private String name;
    private String phoneNumber;
    private String email;
    private String webSite;
    private Address address;
    private List<BloodResourcesResponse> listOfBloodResources;


    public List<BloodResourcesResponse> getListOfBloodResources() {

        return listOfBloodResources;
    }

    public void setListOfBloodResources(List<BloodResourcesResponse> listOfBloodResources) {

        this.listOfBloodResources = listOfBloodResources;
    }


    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }


    public CentersWithResources(String centerId, String name, String phoneNumber, String email, String webSite, Address address, List<BloodResourcesResponse> listOfBloodResources) {
        this.centerId = centerId;
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.webSite = webSite;
        this.email = email;
        this.listOfBloodResources = listOfBloodResources;
    }

    public CentersWithResources() {
    }

}
