package com.eKin.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;

public class Alert implements Parcelable {
    private String alertId;
    private String message;
    private LocalDateTime date;
    private Blood blood;
    private String centerId;

    public Alert(String alertId, String message, LocalDateTime date, Blood blood, String centerId) {
        this.alertId = alertId;
        this.message = message;
        this.date = date;
        this.blood = blood;
        this.centerId = centerId;
    }

    public Alert() {
    }

    private Alert(Parcel in) {
        alertId = in.readString();
        message = in.readString();
        centerId = in.readString();
    }

    public static final Creator<Alert> CREATOR = new Creator<Alert>() {
        @Override
        public Alert createFromParcel(Parcel in) {
            return new Alert(in);
        }

        @Override
        public Alert[] newArray(int size) {
            return new Alert[size];
        }
    };

    public String getAlertId() {
        return alertId;
    }

    public void setAlertId(String alertId) {
        this.alertId = alertId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Blood getBlood() {
        return blood;
    }

    public void setBlood(Blood blood) {
        this.blood = blood;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerUserId) {
        this.centerId = centerUserId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(alertId);
        parcel.writeString(message);
        parcel.writeString(centerId);
    }

    @NotNull
    @Override
    public String toString() {
        return "Alert{" +
                "alertId='" + alertId + '\'' +
                ", message='" + message + '\'' +
                ", date=" + date +
                ", blood=" + blood +
                ", centerUserId='" + centerId + '\'' +
                '}';
    }
}
