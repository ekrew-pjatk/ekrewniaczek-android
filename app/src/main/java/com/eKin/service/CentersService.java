package com.eKin.service;


import com.eKin.requests.responses.CenterResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface CentersService {
    @Headers({"Accept: application/json"})
    @GET("centers")
    Call<List<CenterResponse>> getCenters();
}
