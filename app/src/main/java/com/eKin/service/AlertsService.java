package com.eKin.service;

import com.eKin.requests.responses.AlertResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface AlertsService {
    @Headers({"Accept: application/json"})
    @GET("search")
    Call<List<AlertResponse>> getAlerts(
            @Query("page") int page,
            @Query("groups") List<String> groups,
            @Query("centers") List<String> centers
    );

    @Headers({"Accept: application/json"})
    @GET("search/{id}")
    Call<List<AlertResponse>> getAlert(@Path("id") int id);
}
