package com.eKin.service;

public class ProximityService {
    private static final int RADIUS = 6371;

    public static double calculateDistance(double centerLatitude, double centerLongitude,
                                           double userLatitude, double userLongitude)
    {
        centerLatitude = Math.toRadians(centerLatitude);
        centerLongitude = Math.toRadians(centerLongitude);
        userLatitude = Math.toRadians(userLatitude);
        userLongitude = Math.toRadians(userLongitude);

        double distanceLatitude = userLatitude - centerLatitude;
        double distanceLongitude = userLongitude - centerLongitude;

        double a = Math.pow(Math.sin(distanceLatitude / 2), 2)
                + Math.cos(centerLatitude) * Math.cos(userLatitude)
                * Math.pow(Math.sin(distanceLongitude / 2),2);

        double c = 2 * Math.asin(Math.sqrt(a));

        return(c * RADIUS);
    }
}