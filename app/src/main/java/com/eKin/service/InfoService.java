package com.eKin.service;

import com.eKin.models.Blood;
import com.eKin.models.Center;
import com.eKin.models.CentersWithResources;
import com.eKin.models.DonorsInformation;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface InfoService {

    @Headers({"Accept: application/json"})
    @GET("bloodgroups")
    Call<List<Blood>> getAllBloodGroups();

    @Headers({"Accept: application/json"})
    @GET("centers")
    Call<List<Center>> getAllCenters();

    @Headers({"Accept: application/json"})
    @GET("info/contraindinations")
    Call<List<DonorsInformation>> getAllContraidinations();

    @Headers({"Accept: application/json"})
    @GET("info/circumstances")
    Call<List<DonorsInformation>> getCircumstances();

    @Headers({"Accept: application/json"})
    @GET("centers/centerswithresources")
    Call<List<CentersWithResources>> getAllCentersWithBloodResources();

}
