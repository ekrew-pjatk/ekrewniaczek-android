package com.eKin.service;

import com.eKin.requests.responses.BloodResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface BloodService {

    @Headers({"Accept: application/json"})
    @GET("bloodgroups")
    Call<List<BloodResponse>> getBloodGroups();
}
